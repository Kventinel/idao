import sys
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.multiclass import OneVsRestClassifier
import scipy.sparse
from time import time
from operator import itemgetter
from joblib import Parallel, delayed


def read_data(path):
    dtypes = {'id1': np.int16, 'id2': np.int16, 'id3': np.int16, 'user_id': np.int32, 'date': np.int16}
    train = pd.read_csv(path, dtype=dtypes)
    del train['id1']
    del train['id2']
    del dtypes
    return train


def calculate_target(data, date_test_start, delete_last_three_weeks=False):
    '''
        This function returns a dictionary of type {user: items_list}
        Such that user viewed an item in testing period, 
        but did not view it within the last 3 weeks of train period.
    '''
    
    test_mask = (data.date >= date_test_start) & (data.date < date_test_start + 7)
    if delete_last_three_weeks == True:
        last_3weeks_mask = (data.date >= date_test_start - 21 + 1) & (data.date < date_test_start)
        items_test = data[test_mask].groupby('user_id').id3.apply(set)
        user_last_3weeks = data[last_3weeks_mask].groupby('user_id').id3.apply(set)
        user_last_3weeks = pd.Series()
        joined = items_test.reset_index().merge(user_last_3weeks.reset_index(), on=['user_id'], how='left')
        joined.set_index('user_id', inplace=True)
    else:
        joined = data[test_mask].groupby('user_id').id3.apply(set)
    
    target = {}

    if delete_last_three_weeks == True:
        for user_id, (id3_x, id3_y) in joined.iteritems():   
            items = id3_x if id3_y is np.nan else id3_x - id3_y
            if items != set(): target.update({user_id: items})
    else:
        for user_id, id3_x in joined.iteritems():   
            if id3_x != set(): target.update({user_id: id3_x})

    return target


def get_feats(data, shape):
    '''
        Builds sparse matrix using users' history.
    '''
    return scipy.sparse.coo_matrix(([1] * data.shape[0], (data.user_id, data.id3)), 
                                    shape = shape).tocsr()


def get_train_matrix(data, num_id3):
    data.sort_values(by=['user_id', 'date'], inplace=True)
    data.reset_index(drop=True, inplace=True)
    last_id3 = []
    k = 0
    users_x = []
    id3s_x = []
    users_y = []
    id3s_y = []
    for i in range(1, data.shape[0]):
        if data.loc[i, 'user_id'] == data.loc[i - 1, 'user_id']:
            last_id3.append(data.loc[i - 1, 'id3'])
            if len(last_id3) > 10 and len(last_id3) % 4 == 0:
                users_x += [k] * len(last_id3)
                id3s_x += last_id3
                users_y.append(k)
                id3s_y.append(data.loc[i, 'id3'])
                k += 1
        else:
            last_id3 = []
    return scipy.sparse.coo_matrix(([1] * len(users_x), (users_x, id3s_x)), shape = [k, num_id3]).tocsr(), scipy.sparse.coo_matrix(([1] * len(users_y), (users_y, id3s_y)), shape = [k, num_id3]).tocsr()


def get_target_matrix(X, shape, from_dict=False):
    '''
        Builds sparse matrix using dictionary.
    '''

    if from_dict == True:
        indptr = [0]
        indices = []
        data = []
        vocabulary = {}

        ks = []
        for k in range(shape[0]):
            d = target_dict.get(k, [])
            for y in d:
                indices.append(y)
                data.append(1)
            indptr.append(len(indices))

        return scipy.sparse.csr_matrix((data, indices, indptr), dtype=int, shape =shape)

    matrix = scipy.sparse.coo_matrix(([1] * X.shape[0], (X.user_id, X.id3)), 
                                      shape = shape).tocsr()
    matrix[matrix > 1] = 1
    return matrix


t = time()
path = '../train.csv.zip'
if sys.argv[1] == 'main':
    path = 'train.csv.zip'
train = read_data(path)
num_users = len(train.user_id.unique())
date_validation_start = train.date.max() + 1
if sys.argv[1] == 'test':
    date_validation_start -= 7
    y_val_dict = calculate_target(train, date_validation_start)
date_train_end = date_validation_start - 7

id3_list = list(train.id3.value_counts().keys())
id3_list = id3_list[:len(id3_list) // 7]
train = train[train.id3.isin(id3_list)]
users_list = list(train.user_id.value_counts().keys())
users_list = users_list[:len(users_list)]
train = train[train.user_id.isin(users_list)]

temp = {j: i for i, j in enumerate(id3_list)}
temp3 = {i: j for i, j in enumerate(id3_list)}
train.id3 = [temp[x] for x in train.id3.values]

del temp
del id3_list

temp = {j: i for i, j in enumerate(users_list)}
temp2 = {i: j for i, j in enumerate(users_list)}
train.user_id = [temp[x] for x in train.user_id.values]

del users_list
del temp

features_shape = [train.user_id.max() + 1, train.id3.max() + 1]

# users_mask = train.user_id < num_users / 2
mask_train = (train.date < date_train_end)
mask_test = (train.date < date_validation_start) & (train.date >= train.date.min() + 7)

# X_train, y_train = get_train_matrix(train.loc[mask_train], features_shape[1])
X_train = get_feats(train.loc[mask_train], features_shape)
X_test = get_feats(train.loc[mask_test], features_shape)
# y_train_dict = calculate_target(train, date_validation_start - 7)
y_train = get_target_matrix(train[(date_validation_start - 7 <= train.date) & (train.date < date_validation_start)], features_shape)
# y_train = get_target_matrix(train[(date_train_end <= train.date) & (train.date < date_train_end + 7)], features_shape)

# del users_mask
del mask_train

clf = OneVsRestClassifier(LogisticRegression(max_iter=20), n_jobs=-1)
clf.fit(X_train,y_train)

del X_train
del y_train

preds=clf.predict_proba(X_test)

del clf
del X_test

num = int(np.ceil(num_users * 0.05))
ans_inds = np.argsort(preds)

last_3weeks = train.loc[mask_test].loc[train.loc[mask_test].date >= train.loc[mask_test].date.max() - 21 + 1]
y_not = last_3weeks.groupby('user_id').id3.apply(set)

del train
del mask_test
del last_3weeks

y_pred = {}
sorted_data = []
dicts = []

def get_preds(user):
    items_not = y_not.get(user, [])
    items_pred = []
    i = 1
    prob = 1
    while len(items_pred) < 5 and i < features_shape[1]:
        if not ans_inds[user, -i] in items_not:
            items_pred += [temp3[ans_inds[user, -i]]]
            prob *= 1 - preds[user, ans_inds[user, -i]]
    
        i += 1
    if len(items_pred) < 5:
        prob = 1
    return ((prob, user), {temp2[user]: items_pred})

sorted_data = np.array(Parallel(n_jobs = -1, verbose=50)(delayed(get_preds)(i) for i in range(features_shape[0])))
dicts = sorted_data[:, 1]
sorted_data = list(sorted_data[:, 0])

del preds

sorted_data = sorted(sorted_data)

for pair in sorted_data[:num]:
    y_pred.update(dicts[pair[1]])

if sys.argv[1] == 'test':
    sys.path.append('../')
    from scorer import scorer
    print(scorer(y_val_dict, y_pred, num_users))
else:
    y_pred_df = pd.DataFrame.from_records(y_pred).T.reset_index()
    y_pred_df.columns = ['user_id', 'id3_1', 'id3_2', 'id3_3', 'id3_4', 'id3_5']

    y_pred_df.to_csv('submission.csv', index=False)

print(time() - t)
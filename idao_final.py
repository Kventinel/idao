import pandas as pd
import sys
import time
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import ExtraTreesRegressor
import tqdm
import xgboost as xgb


start_time = time.time()
leap_year = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
unleap_year = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


train = pd.read_csv('train.csv.zip')
if sys.argv[1] == 'test':
  pred_dates  = ['2017-08-16', '2017-08-17', '2017-08-18', '2017-08-19',
                 '2017-08-20', '2017-08-21', '2017-08-22', '2017-08-23',
                 '2017-08-24', '2017-08-25', '2017-08-26', '2017-08-27',
                 '2017-08-28', '2017-08-29', '2017-08-30', '2017-08-31',
                 '2017-09-01', '2017-09-02', '2017-09-03', '2017-09-04',
                 '2017-09-05', '2017-09-06', '2017-09-07', '2017-09-08',
                 '2017-09-09', '2017-09-10', '2017-09-11', '2017-09-12',
                 '2017-09-13', '2017-09-14', '2017-09-15', '2017-09-16',
                 '2017-09-17']
if sys.argv[1] == 'train':
  train = train[train['DATE'] <= '2017-07-13']
  pred_dates  = ['2017-07-14', '2017-07-15', '2017-07-16', '2017-07-17',
                 '2017-07-18', '2017-07-19', '2017-07-20', '2017-07-21',
                 '2017-07-22', '2017-07-23', '2017-07-24', '2017-07-25',
                 '2017-07-26', '2017-07-27', '2017-07-28', '2017-07-29',
                 '2017-07-30', '2017-07-31', '2017-08-01', '2017-08-02',
                 '2017-08-03', '2017-08-04', '2017-08-05', '2017-08-06',
                 '2017-08-07', '2017-08-08', '2017-08-09', '2017-08-10',
                 '2017-08-11', '2017-08-12', '2017-08-13', '2017-08-14',
                 '2017-08-15']


def get_week_day(date):
  year = int(date[:4])
  month = int(date[5:7])
  day = int(date[8:])
  week_day = day - 1
  if year == 2016:
    week_day += 365 + sum(leap_year[:month - 1])
  else:
    week_day += sum(unleap_year[:month - 1])
    if year == 2017:
      week_day += 365 + 366
  return week_day % 7


def get_day(date):
  year = int(date[:4])
  month = int(date[5:7])
  day = int(date[8:])
  week_day = day - 1
  if year == 2016:
    week_day += 365 + sum(leap_year[:month - 1])
  else:
    week_day += sum(unleap_year[:month - 1])
    if year == 2017:
      week_day += 365 + 366
  return week_day


def get_date(day):
  month = 0
  if day >= 365 and day < 365 + 366:
    year = 2016
    day -= 365
    while day >= leap_year[month]:
        day -= leap_year[month]
        month += 1
  else:
    if day < 365:
      year = 2015
    else:
      year = 2017
      day -= 365 + 366
    while day >= unleap_year[month]:
        day -= unleap_year[month]
        month += 1
  day = str(day + 1).zfill(2)
  month = str(month + 1).zfill(2)
  return str(year) + '-' + month + '-' + day



def sub_days(date, days):
  day = get_day(date)
  return get_date(day - days)


def add_days(date, days):
  day = get_day(date)
  return get_date(day + days)


def get_mean(values):
  return np.mean(sorted(values)[2:-2])


# Unique ATMs

def heuristic(t):
  train = t.copy()
  ATM_IDs = sorted(train.ATM_ID.unique())
  train.loc[:, 'WEEK_DAY'] = [get_day(date) % 7 for date in train['DATE']]
  rows = np.zeros((len(t.ATM_ID.unique()), 33))
  for j, ATM in enumerate(ATM_IDs):
    ATM_data = train[train['ATM_ID'] == ATM]
    ATM_data.sort_values('DATE', inplace=True, ascending=False)
    ATM_data = ATM_data.CLIENT_OUT.values
    u = 63
    ATM_data = ATM_data[:u]
    atm_mean = [get_mean(ATM_data[6 - week_day::7])
                for week_day in range(7)]
    for i in range(len(pred_dates)):
      rows[j][i] = atm_mean[i % 7]
  return rows





def heuristic_trand(t):
  data1 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data1[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  for j in range(data1.shape[0]):
    u = 63
    ATM_data = data1[j, -u-33:-33]
    atm_mean = [get_mean(ATM_data[week_day::7])
                for week_day in range(7)]
    trand_delta = np.mean(sorted([ATM_data[day+7:day+14].mean() - ATM_data[day:day+7].mean() for day in range(0, 49)])[5:-5])
    for i in range(len(pred_dates)):
      data1[j][-33+i] = atm_mean[i % 7] + (i - 4) / 49 * trand_delta
  return data1[:, -33:]







def heuristic2(t):
  train = t.copy()
  ATM_IDs = train.ATM_ID.unique()
  test = train[train['DATE'] > sub_days(train['DATE'].max(), 99)]
  train_Y = train[train['DATE'] > sub_days(train['DATE'].max(), 33)]
  train = train[(train['DATE'] > sub_days(train['DATE'].max(), 99)) & (train['DATE'] <= sub_days(train['DATE'].max(), 33))]
  first_month = train[train['DATE'] <= sub_days(train['DATE'].max(), 33)]
  second_month = train[train['DATE'] > sub_days(train['DATE'].max(), 33)]
  train_Y.loc[:, 'WEEK_DAY'] = [get_week_day(date) for date in train_Y['DATE']]
  train.loc[:, 'WEEK_DAY'] = [get_week_day(date) for date in train['DATE']]
  test.loc[:, 'WEEK_DAY'] = [get_week_day(date) for date in test['DATE']]

  X = np.zeros((len(ATM_IDs) * 33, 3))
  Y = np.zeros(len(ATM_IDs) * 33)
  for i, ATM in enumerate(ATM_IDs):
    atm_mean = [train.loc[(train['ATM_ID'] == ATM) & (train['WEEK_DAY'] == week_day), 
                          'CLIENT_OUT'].mean()
                for week_day in range(7)]
    atm1 = second_month[second_month['ATM_ID'] == ATM].CLIENT_OUT.mean()
    atm2 = first_month[first_month['ATM_ID'] == ATM].CLIENT_OUT.mean()
    for j, date in enumerate(train_Y[train_Y['ATM_ID'] == ATM].DATE.values):
      day = get_week_day(date)
      X[i * 33 + j] = [atm_mean[day], atm1, atm2]
      Y[i * 33 + j] = train_Y[(train_Y['ATM_ID'] == ATM) & (train_Y['DATE'] == date)].CLIENT_OUT.values[0]
  clf = LinearRegression(n_jobs=-1)
  clf.fit(X, Y)

  first_month = test[test['DATE'] <= sub_days(test['DATE'].max(), 33)]
  second_month = test[test['DATE'] > sub_days(test['DATE'].max(), 33)]

  rows = np.zeros((len(ATM_IDs) * 33, 3))
  dates = []
  atms = []
  for i, ATM in enumerate(ATM_IDs): 
    atm_mean = [test.loc[(test['ATM_ID'] == ATM) & (test['WEEK_DAY'] == week_day), 
                         'CLIENT_OUT'].mean()
                for week_day in range(7)]
    atm1 = second_month[second_month['ATM_ID'] == ATM].CLIENT_OUT.mean()
    atm2 = first_month[first_month['ATM_ID'] == ATM].CLIENT_OUT.mean()
    for j, date in enumerate(pred_dates):
      day = get_week_day(date)
      rows[i * 33 + j] = [atm_mean[day], atm1, atm2]
      dates.append(date)
      atms.append(ATM)
  rows = clf.predict(rows)
  return list(zip(dates, atms, rows))





def heuristic3(t):
  data1 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data2 = np.zeros(data1.shape)
  data3 = np.zeros(data1.shape)
  data1[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  data2[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  eps_day = 0.06
  eps_week = 0.07
  eps_month = 0.2
  alpha = 0.99
  beta = 1.01
  gamma = 0.
  for i in range(1, len(t.DATE.unique())):
    data1[:, i] = data1[:, i - 1] * (1 - eps_day) + data1[:, i] * eps_day
  for i in range(7, len(t.DATE.unique())):
    data2[:, i] = data2[:, i - 7] * (1 - eps_week) + (data2[:, i] - data1[:, i]) * eps_week
  for i in range(28, len(t.DATE.unique())):
    data3[:, i] = data3[:, i - 28] * (1 - eps_month) + (data3[:, i] - data2[:, i] - data1[:, i]) * eps_month
  res = np.zeros((len(t.ATM_ID.unique()), 33))
  for i in range(-33, 0):
    res[:, i+33] = alpha * data1[:, i-1] + beta * data2[:, i-7] + gamma * data3[:, i-28]
    data1[:, i] = data1[:, i - 1] * (1 - eps_day) + res[:, i+33] * eps_day
    data2[:, i] = data2[:, i - 7] * (1 - eps_week) + (res[:, i+33] - data1[:, i]) * eps_week
    data3[:, i] = data3[:, i - 28] * (1 - eps_month) + (res[:, i+33] - data2[:, i] - data1[:, i]) * eps_month
  return res




def heuristic4(t):
  data = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  for i in range(-33, 0):
    week_sums = np.array([data[:, i-7*j-10:i-7*j-3].mean(axis=1) for j in range(9)])
    day_sums = np.array([data[:, i-7*j-7] for j in range(8)])
    data[:, i] = np.average(week_sums, axis=0) + np.average(day_sums - week_sums[1:], axis=0)
  return data[:, -33:]





def heuristic5(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_weeks = 9
  n_days = 49
  for i in range(-33, 0):
    trand_line = np.subtract(data3[:, i-n_days:i].mean(axis=1), data3[:, i-2*n_days:i-n_days].mean(axis=1))
    mean_val = data3[:, i-2*n_days:i].mean(axis=1)
    bias = np.subtract(data3[:, i-n_weeks*7:i:7].mean(axis=1), data3[:, i-n_weeks*7-3:i-3].mean(axis=1))
    data3[:, i] = mean_val + trand_line + bias
  return data3[:,-33:]





def linearregressor():
  max_date = train['DATE'].max()
  max_day = 330
  train = train[train['DATE'] > sub_days(max_date, max_day)]
  max_day -= 33
  max_day //= 7
  min_day = 5
  train['DATE'] = [get_day(date) for date in train['DATE']]
  if sys.argv[1] == 'test':
    clf = LinearRegression()
  else:  
    max_date = train['DATE'].max()
    train_X = np.zeros((33 * len(ATM_IDs), max_day - min_day))
    train_Y = np.zeros(33 * len(ATM_IDs))
    for i, ATM in enumerate(ATM_IDs):
      ATM_data = train[train['ATM_ID'] == ATM]
      for j in range(33):
        train_X[i * 33 + j] = [ATM_data[ATM_data['DATE'] == max_date - 32 + j - k * 7].CLIENT_OUT.values[0]
                               for k in range(min_day, max_day)]
        train_Y[i * 33 + j] = ATM_data[ATM_data['DATE'] == max_date - 32 + j].CLIENT_OUT.values[0]

    clf = LinearRegression(n_jobs=-1)
    clf.fit(train_X, train_Y)

  test_X = np.zeros((len(ATM_IDs) * 33, max_day - min_day))
  atms = []
  dates = []
  for i, ATM in enumerate(ATM_IDs): 
    ATM_data = train[train['ATM_ID'] == ATM]
    for j, date in enumerate(pred_dates):
      test_X[i * 33 + j] = [ATM_data[ATM_data['DATE'] == get_day(date) - k * 7].CLIENT_OUT.values[0]
                            for k in range(min_day, max_day)]
      atms.append(ATM)
      dates.append(date)

  return list(zip(dates, atms, clf.predict(test_X)))





def regressor2(train):
  ATM_IDs = train.ATM_ID.unique()
  train.loc[:, 'DATE'] = [get_day(date) for date in train.loc[:, 'DATE']]
  train = train[train['DATE'] > train.DATE.values.max() - 924]
  train.sort_values(['ATM_ID', 'DATE'], inplace=True)
  n_features = 91
  test = train[train['DATE'] > train.DATE.values.max() - n_features]
  for i in range(train.DATE.values.min() + n_features, train.DATE.values.max() - n_features, 7):
    y_train = train[(train['DATE'] > i) & (train['DATE'] <= i + 33)]
    x_train = train[(train['DATE'] > i - n_features) & (train['DATE'] <= i)]
    if i == train.DATE.values.min() + n_features:
      X_train = np.array([x_train[x_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])
      Y_train = np.array([y_train[y_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])
    else:
      X_train = np.concatenate((X_train, np.array([x_train[x_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])))
      Y_train = np.concatenate((Y_train, np.array([y_train[y_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])))
  regressors = [LinearRegression(n_jobs=-1) for i in range(33)]
  for i in range(33):
    regressors[i].fit(X_train, Y_train[:, i])
  X_test = np.array([test[test['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])
  Y_test = []
  for i in range(33):
    Y_test.append(regressors[i].predict(X_test))
  rows = []
  for j, ATM in enumerate(ATM_IDs):
    for i, date in enumerate(pred_dates):
      rows.append([date, ATM, Y_test[i][j]])
  return rows



def final(train):
  train.loc[:, 'DATE'] = [get_day(date) for date in train.loc[:, 'DATE']]
  train = train[train['DATE'] > train.DATE.values.max() - 280]
  train.sort_values(['ATM_ID', 'DATE'], inplace=True)
  ATM_IDs = train.ATM_ID.unique()
  n_features = 49
  test = train[train['DATE'] > train.DATE.values.max() - n_features]
  for i in range(train.DATE.values.min() + n_features, train.DATE.values.max() - 32, 7):
    y_train = train[(train['DATE'] >= i) & (train['DATE'] < i + 33)]
    x_train = train[(train['DATE'] >= i - n_features) & (train['DATE'] < i)]
    if i == train.DATE.values.min() + n_features:
      XXX = np.array([x_train[x_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])
      X_train = np.zeros((XXX.shape[0], 8))
      X_train[:, 0] = XXX.mean(axis=1)
      X_train[:, 1:] = [[ATM[i::7].mean() for i in range(7)] for ATM in XXX]
      Y_train = np.array([y_train[y_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])
    else:
      XXX = np.array([x_train[x_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])
      temp = np.zeros((XXX.shape[0], 8))
      temp[:, 0] = XXX.mean()
      temp[:, 1:] = [[ATM[i::7].mean() for i in range(7)] for ATM in XXX]
      X_train = np.concatenate((X_train, temp))
      Y_train = np.concatenate((Y_train, np.array([y_train[y_train['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])))
  regressors = [LinearRegression(n_jobs=-1) for i in range(33)]
  for i in range(33):
    regressors[i].fit(X_train, Y_train[:, i])
  XXX = np.array([test[test['ATM_ID'] == ATM].CLIENT_OUT.values for ATM in ATM_IDs])
  X_test = np.zeros((XXX.shape[0], 8))
  X_test[:, 0] = XXX.mean(axis=1)
  X_test[:, 1:] = [[ATM[i::7].mean() for i in range(7)] for ATM in XXX]
  Y_test = []
  for i in range(33):
    Y_test.append(regressors[i].predict(X_test))
  rows = []
  for j, ATM in enumerate(ATM_IDs):
    for i, date in enumerate(pred_dates):
      rows.append([date, ATM, Y_test[i][j]])
  return rows




def zero_classifier(t, pred):
  data3 = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  dates = sorted(t.DATE.unique())
  while len(dates) % 7 != 0:
    del data3[dates[0]]
    del dates[0]
  n_features = 42
  X = data3.values[:, :n_features]
  Y = data3.values[:, n_features:n_features+33]
  for i in range(7, data3.shape[1] - n_features - 33, 7):
    X = np.concatenate((X, data3.values[:, i:i+n_features]))
    Y = np.concatenate((Y, data3.values[:, i+n_features:i+n_features+33]))
  Y[Y > 0] = 1
  K = np.zeros((data3.shape[0], 33))
  for i in tqdm.tqdm(range(33)):
    clf = xgb.XGBClassifier(n_jobs=-1, max_depth=2, learning_rate=0.05)
    clf.fit(X, Y[:, i])
    # K[:, i] = clf.predict_proba(data3.values[:, -n_features:])[:, 1] * clf.predict(data3.values[:, -n_features:])
    K[:, i] = clf.predict(data3.values[:, -n_features:])
  K[K == 0] = 0.5
  return pred * K
  





def zero_classifier2(t, predict):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_features = 56
  X = [data3[:, i:i+n_features] for i in range(7)]
  Y = [data3[:, i+n_features] for i in range(7)]
  for i in range(7, data3.shape[1] - n_features - 33):
    X[i % 7] = np.concatenate((X[i % 7], data3[:, i:i+n_features]))
    Y[i % 7] = np.concatenate((Y[i % 7], data3[:, i+n_features]))
  clfs = []
  for i in tqdm.tqdm(range(7)):
    Y[i][Y[i] > 0] = 1
    clfs.append(xgb.XGBClassifier(n_jobs=-1, max_depth=2, learning_rate=0.05))
    clfs[-1].fit(X[i], Y[i])
  for i in range(data3.shape[1] - 33, data3.shape[1]):
    data3[:, i] = predict[:, i - data3.shape[1]] * clfs[i % 7].predict_proba(data3[:, i-n_features:i])[:, 1] * clfs[i % 7].predict(data3[:, i-n_features:i])
  return data3[:, -33:]





def zero_classifier3(t, pred):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_features = 56
  X = data3[:, :n_features]
  Y = data3[:, n_features]
  for i in range(1, data3.shape[1] - n_features - 33):
    X = np.concatenate((X, data3[:, i:i+n_features]))
    Y = np.concatenate((Y, data3[:, i+n_features]))
  clf = xgb.XGBClassifier(n_jobs=-1, max_depth=2, learning_rate=0.01, n_estimators=400)
  # clf = RandomForestClassifier(n_jobs=-1)
  Y[Y > 0] = 1
  Y[Y != 1] = 0
  clf.fit(X, Y)
  for i in range(-33, 0):
    temp = clf.predict(data3[:, i-n_features:i])
    temp[temp == 0] = 0.7
    data3[:, i] = pred[:, i] * temp
  return data3[:, -33:]




def final2(t):
  data3 = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  dates = sorted(t.DATE.unique())
  while len(dates) % 7 != 0:
    del data3[dates[0]]
    del dates[0]
  n_features = 49
  X = data3.values[:, :n_features]
  Y = data3.values[:, n_features:n_features+33]
  for i in range(n_features, data3.shape[1] - n_features - 33, 7):
    X = np.concatenate((X, data3.values[:, i:i+n_features]))
    Y = np.concatenate((Y, data3.values[:, i+n_features:i+n_features+33]))
  K = np.zeros((data3.shape[0], 33))
  for i in tqdm.tqdm(range(33)):
    clf = LinearRegression(n_jobs=-1)
    clf.fit(X, Y[:, i])
    K[:, i] = clf.predict(data3.values[:, -n_features:])
  return K




def final3(t):
  data3 = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT')
  n_features = 175
  j_weeks = [1, 2, 4, 7, 10, 15, 20, 25]
  X = [data3.values[:, n_features-j*7:n_features:7].mean(axis=1) for j in j_weeks]
  Y = data3.values[:, n_features]
  for i in range(n_features, data3.shape[1] - n_features):
    X = np.concatenate((X, [data3.values[:, i+n_features-j*7:i+n_features:7].mean(axis=1) for j in j_weeks]), axis=1)
    Y = np.concatenate((Y, data3.values[:, i+n_features]))
  clf = ExtraTreesRegressor(n_jobs=-1)
  clf.fit(X.T, Y)
  K = np.zeros((data3.shape[0], 33))
  for i in range(33):
    K[:, i] = clf.predict(np.array([data3.values[:, -j*7+(i%7)::7].mean(axis=1) for j in j_weeks]).T)
  return K





def final4(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_weeks = 12
  n_features = n_weeks * 7
  X1 = [data3[:, n_features-j*7-7:n_features-j*7].mean(axis=1) for j in range(n_weeks)]
  X2 = [data3[:, n_features-(j+1)*7] for j in range(n_weeks)]
  Y = data3[:, n_features]
  for i in range(1, data3.shape[1] - 33 - n_features):
    X1 = np.concatenate((X1, [data3[:, i+n_features-j*7-7:i+n_features-j*7].mean(axis=1) for j in range(n_weeks)]), axis=1)
    X2 = np.concatenate((X2, [data3[:, i+n_features-(j+1)*7] for j in range(n_weeks)]), axis=1)
    Y = np.concatenate((Y, data3[:, i+n_features]))
  X1 = X1.T
  X2 = X2.T
  Y -= np.mean(np.subtract(X2, X1), axis=1)
  clf = LinearRegression(n_jobs=-1)
  clf.fit(X1, Y)
  for i in range(-33, 0):
    X1 = np.array([data3[:, i-j*7-7:i-j*7].mean(axis=1) for j in range(n_weeks)]).T
    X2 = np.array([data3[:, i-j*7-7] for j in range(n_weeks)]).T
    Y = np.mean(np.subtract(X2, X1), axis=1)
    data3[:, i] = clf.predict(X1) + Y
  return data3[:,-33:]





def stupid_heuristic(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  for i in range(-33, 0):
    data3[:, i] = np.minimum(data3[:, i - 7], data3[:, i - 14])
  return data3[:, -33:]





def stupid_heuristic2(t):
  data = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  for i in range(-33, 0):
    data[:, i] = (np.mean(data[:, i-3:i], axis=1) / np.mean(data[:, i-10:i-7], axis=1) - 1 ) * np.mean(data[:, i-10:i-3], axis=1) + data[:, i-7]
  data = np.nan_to_num(data)
  print(data)
  return data[:, -33:]




def may_be(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_features = 70
  n_first = 3
  X = data3[:, n_features-n_first:n_features]
  X2 = data3[:, :n_features].mean(axis=1)
  X3 = np.array([data3[:, n_features-j:n_features-n_first].mean(axis=1) for j in range(n_first + 7, n_features, 7)]).T
  X4 = np.cumsum(data3[:, :n_features:7][:, :0:-1], axis=1) / np.arange(1, 10)
  print(X3.shape, X4.shape)
  Y = data3[:, n_features]
  for i in range(1, data3.shape[1] - n_features - 33):
    X = np.concatenate((X, data3[:, i+n_features-n_first:i+n_features]))
    X2 = np.concatenate((X2, data3[:, i:i+n_features].mean(axis=1)))
    X3 = np.concatenate((X3, np.array([data3[:, i+n_features-j:i+n_features-n_first].mean(axis=1) for j in range(n_first + 7, n_features, 7)]).T))
    X4 = np.concatenate((X4, np.cumsum(data3[:, i:i+n_features:7][:, :0:-1], axis=1) / np.arange(1, 10)))
    Y = np.concatenate((Y, data3[:, i+n_features]))
  clf = xgb.XGBRegressor(n_jobs=-1)
  X = np.concatenate((X, np.array([X2]).T), axis=1)
  X = np.concatenate((X, X3), axis=1)
  X = np.concatenate((X, X4), axis=1)
  X = np.concatenate((X, X4 / X3), axis=1)
  X = np.nan_to_num(X)
  print((X4 / X3)[0])
  clf.fit(X, Y)
  for i in range(-33, 0):
    X = data3[:, i-n_first:i]
    X2 = data3[:, i-n_features:i].mean(axis=1)
    X3 = np.array([data3[:, i-j:i-n_first].mean(axis=1) for j in range(n_first + 7, n_features, 7)]).T
    X4 = np.cumsum(data3[:, i-n_features:i:7][:, :0:-1], axis=1) / np.arange(1, 10)
    X = np.concatenate((X, np.array([X2]).T), axis=1)
    X = np.concatenate((X, X3), axis=1)
    X = np.concatenate((X, X4), axis=1)
    X = np.concatenate((X, X4 / X3), axis=1)
    X = np.nan_to_num(X)
    Y = clf.predict(X)
    data3[:, i] = Y
  return data3[:, -33:]







def may_be2(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_features = 441
  temp = [[] for i in range(33)]
  for i in range(-33, 0):
    for k in range(7):
      X5 = np.array([data3[:, i-j:i-j+21].mean(axis=1) for j in range(21, n_features+1, 21)]).T
      X3 = np.cumsum(np.array([data3[:, i-j:i-j+7].mean(axis=1) for j in range(7, n_features+1, 7)]).T, axis=1)
      X4 = np.cumsum(data3[:, i-n_features+k:i:7][:, ::-1], axis=1)
      X3 /= np.arange(1, n_features // 7 + 1)
      X4 /= np.arange(1, n_features // 7 + 1)
      X3 = np.maximum(X3, 1)
      X = X4 / X3
      SUM = X3[:, :6].mean(axis=1)
      X2 = X.copy()
      X = X[:,-1]
      aabs = 40000
      trand = np.minimum(aabs, np.maximum(-aabs, data3[:, -28+i:i].mean(axis=1) - data3[:, -56+i:-28+i].mean(axis=1)))
      if i + k < 0:
        temp[i+k].append((SUM + trand * (14 + k) / 150) * X)
        for l in range(data3.shape[0]):
          u = 0
          for z in range(1, X5.shape[1]):
            if max(X5[l][z], X5[l][0]) > 20 * min(X5[l][z], X5[l][0]):
              u = z * 3
              break
          if u > 0:
            temp[i + k][-1][l] = (SUM[l] + trand[l] * (14 + k) / 150) * X2[l, :u].mean()
    data3[:, i] = np.min(temp[i], axis=0)
  return data3[:, -33:]





def may_be2_ml(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_features = 70
  X1 = np.array([data3[:, n_features-j:n_features-j+7].mean(axis=1) for j in range(7, n_features+1)]).T
  # X2 = data3[:, :n_features:7][:, ::-1]
  Y1 = data3[:, n_features-6:n_features+1].mean(axis=1)
  # Y2 = data3[:, n_features]
  for i in range(1, data3.shape[1] - 33 - n_features - 1):
    X1 = np.concatenate((X1, np.array([data3[:, i+n_features-j:i+n_features-j+7].mean(axis=1) for j in range(7, n_features+1)]).T))
    # X2 = np.concatenate((X2, data3[:, i:i+n_features:7][:, ::-1]))
    Y1 = np.concatenate((Y1, data3[:, i+n_features-6:i+n_features+1].mean(axis=1)))
    # Y2 = np.concatenate((Y2, data3[:, i+n_features]))
  X1 = np.cumsum(X1, axis=1) / np.arange(1, X1.shape[1] + 1)
  # X2 = np.cumsum(X2, axis=1) / np.arange(1, n_features // 7 + 1)
  X1 = np.maximum(X1, 1)
  Y1 = np.maximum(Y1, 1)
  clf1 = LinearRegression(n_jobs=-1)
  # clf2 = xgb.XGBRegressor(n_jobs=-1, n_estimators=100)
  clf1.fit(X1, Y1)
  # clf2.fit(X1, Y1)
  # X2 /= X1
  # Y2 /= Y1
  # clf2 = LinearRegression(n_jobs=-1)
  # clf2 = xgb.XGBRegressor(n_jobs=-1, n_estimators=1000)
  # clf2.fit(X2, Y2)
  for i in range(-33, 0):
    X1 = np.array([data3[:, i-j:i-j+7].mean(axis=1) for j in range(7, n_features+1)]).T
    # X2 = np.array(data3[:, i-n_features:i:7][:, ::-1])
    X1 = np.cumsum(X1, axis=1) / np.arange(1, X1.shape[1] + 1)
    # X2 = np.cumsum(X2, axis=1) / np.arange(1, n_features // 7 + 1)
    X1 = np.maximum(X1, 1)
    # X2 /= X1
    Y1 = np.maximum(np.mean([clf1.predict(X1), clf1.predict(X1)], axis=0), 1)
    # Y2 = np.maximum(clf2.predict(X2), 0)
    # data3[:, i] = Y1 * Y2
    data3[:, i] = Y1 * 7 - data3[:, i-6:i].sum(axis=1)
  return data3[:, -33:]





def may_be3(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_features = 98
  temp = [[] for i in range(33)]
  for i in range(-33, 0):
    for k in range(7):
      X3 = np.cumsum(np.array([data3[:, i-j:i-j+7].mean(axis=1) for j in range(7, n_features+1, 7)]).T, axis=1)
      X4 = np.cumsum(data3[:, i-n_features+k:i:7][:, ::-1], axis=1)
      X3 /= np.arange(1, n_features // 7 + 1)
      X4 /= np.arange(1, n_features // 7 + 1)
      X3 = np.maximum(X3, 1)
      X = X4 - X3
      SUM = X3[:, :10].mean(axis=1)
      X = np.mean(X, axis=1)
      if i + k < 0:
        temp[i+k].append(SUM + X)
    data3[:, i] = np.min(temp[i], axis=0)
  return data3[:, -33:]






def final_ml(t):
  data3 = np.zeros((len(t.ATM_ID.unique()), len(t.DATE.unique()) + 33))
  data3[:, :-33] = t.pivot(index='ATM_ID', columns='DATE', values='CLIENT_OUT').values
  n_features = 70
  means = np.array([data3[:, n_features-j:n_features-j+7].mean(axis=1) for j in range(7, n_features+1, 7)]).T
  X1 = np.cumsum(means, axis=1) / np.arange(1, n_features // 7 + 1)
  X2 = np.cumsum(data3[:, :n_features:7][:, ::-1], axis=1) / np.arange(1, n_features // 7 + 1)
  X3 = np.array([np.median(means[:, :q+1], axis=1) for q in range(2, means.shape[1])]).T
  X4 = np.array([np.median(data3[:, :n_features:7][:, ::-1][:, :q+1], axis=1) for q in range(2, means.shape[1])]).T
  X5
  Y = np.array(data3[:, n_features])
  for i in range(1, data3.shape[1] - 33 - n_features):
    means = np.array([data3[:, i+n_features-j:i+n_features-j+7].mean(axis=1) for j in range(7, n_features+1, 7)]).T
    X1 = np.concatenate((X1, np.cumsum(means, axis=1) / np.arange(1, n_features // 7 + 1)))
    X2 = np.concatenate((X2, np.cumsum(data3[:, i:i+n_features:7][:, ::-1], axis=1) / np.arange(1, n_features // 7 + 1)))
    X3 = np.concatenate((X3, np.array([np.median(means[:, :q+1], axis=1) for q in range(2, means.shape[1])]).T))
    X4 = np.concatenate((X4, np.array([np.median(data3[:, i:i+n_features:7][:, ::-1][:, :q+1], axis=1) for q in range(2, means.shape[1])]).T))
    Y = np.concatenate((Y, data3[:, i + n_features]))
  clf = LinearRegression(n_jobs=-1)
  X = np.concatenate((X1, X2, X3, X4), axis=1)
  clf.fit(X, Y)
  for i in range(-33, 0):
    means = np.array([data3[:, i-j:i-j+7].mean(axis=1) for j in range(7, n_features+1, 7)]).T
    X1 = np.cumsum(means, axis=1) / np.arange(1, n_features // 7 + 1)
    X2 = np.cumsum(data3[:, i-n_features:i:7][:, ::-1], axis=1) / np.arange(1, n_features // 7 + 1)
    X3 = np.array([np.median(means[:, :q+1], axis=1) for q in range(2, means.shape[1])]).T
    X4 = np.array([np.median(data3[:, i-n_features:i:7][:, ::-1][:, :q+1], axis=1) for q in range(2, means.shape[1])]).T
    X = np.concatenate((X1, X2, X3, X4), axis=1)
    Y = clf.predict(X)
    data3[:, i] = Y
  return data3[:, -33:]







train.loc[train['CLIENT_OUT'] < 0, 'CLIENT_OUT'] = 0
rows = []
# preds = final_ml(train).reshape(-1)
preds = may_be2(train) * 9 + heuristic(train) * 6
preds = zero_classifier(train, preds)
preds = preds.reshape(-1)
koef1 = preds.mean()
koef2 = train[train.DATE > sub_days(train.DATE.max(), 66)].CLIENT_OUT.mean()
preds *= koef2 * 0.84 / koef1
print(koef1, koef2)
i = 0
for atm in train.ATM_ID.unique():
  for date in pred_dates:
    rows.append([date, atm, preds[i]])
    i += 1
submission = pd.DataFrame(rows, columns = ['DATE', 'ATM_ID', 'CLIENT_OUT'])                    
submission.to_csv('submission.csv', index=False)
print(time.time() - start_time)
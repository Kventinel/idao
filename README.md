The submission webpage is located [here](https://official.contest.yandex.com/idao/contest/6919/enter/).  
Username: idao-581  
Password: E38CvNx2ka  
Team name: SHADow of the BSU  
For any questions: hello@idao.world  
To keep up to date on IDAO news, follow us here: [Facebook](https://world.us17.list-manage.com/track/click?u=63278d452e1b977235b4e4cc8&id=4c7f16c08c&e=b17ee0c951) & [VK](https://world.us17.list-manage.com/track/click?u=63278d452e1b977235b4e4cc8&id=0267730159&e=b17ee0c951)  
Download data: [Public](https://www.dropbox.com/s/v71xw29hqt4qykb/train.csv.zip?dl=1), [Private](https://yadi.sk/d/4dPrrlUK3SGCzV)
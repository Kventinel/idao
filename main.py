import sys
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OneVsRestClassifier
import scipy.sparse
from time import time
from operator import itemgetter
from joblib import Parallel, delayed


def read_data(path):
    dtypes = {'id1': np.int16, 'id2': np.int16, 'id3': np.int16, 'user_id': np.int32, 'date': np.int16}
    train = pd.read_csv(path, dtype=dtypes)
    del train['id1']
    del train['id2']
    del dtypes
    return train


def calculate_target(data, date_test_start):
    '''
        This function returns a dictionary of type {user: items_list}
        Such that user viewed an item in testing period, 
        but did not view it within the last 3 weeks of train period.
    '''
    
    test_mask = (data.date >= date_test_start) & (data.date < date_test_start + 7)
    joined = data[test_mask].groupby('user_id').id3.apply(set)
    
    target = {}

    for user_id, id3_x in joined.iteritems():   
        if id3_x != set(): target.update({user_id: id3_x})

    return target


def get_feats(data, shape):
    '''
        Builds sparse matrix using users' history.
    '''
    return scipy.sparse.coo_matrix(([1] * data.shape[0], (data.user_id, data.id3)), 
                                    shape = shape).tocsr()


def get_target_matrix(X, shape):
    '''
        Builds sparse matrix using dictionary.
    '''

    matrix = scipy.sparse.coo_matrix(([1] * X.shape[0], (X.user_id, X.id3)), 
                                      shape = shape).tocsr()
    matrix[matrix > 1] = 1
    return matrix


t = time()
path = 'train.csv.zip'
train = read_data(path)
num_users = len(train.user_id.unique())
date_validation_start = train.date.max() + 1
if sys.argv[1] == 'test':
    date_validation_start -= 7
    y_val_dict = calculate_target(train, date_validation_start)
date_train_end = date_validation_start - 7

id3_list = list(train.id3.value_counts().keys())
id3_list = id3_list[:len(id3_list) // 100]
train = train[train.id3.isin(id3_list)]
temp = {j: i for i, j in enumerate(id3_list)}
temp3 = {i: j for i, j in enumerate(id3_list)}
train.id3 = [temp[x] for x in train.id3.values]

del temp
del id3_list

features_shape = [train.user_id.max() + 1, train.id3.max() + 1]

mask_train = (train.date < date_train_end)
mask_test = (train.date < date_validation_start) & (train.date >= train.date.min() + 7)

X_train = get_feats(train.loc[mask_train], features_shape)
X_test = get_feats(train.loc[mask_test], features_shape)
y_train = get_target_matrix(train[(date_validation_start - 7 <= train.date) & (train.date < date_validation_start)], features_shape)

del mask_train

clf = OneVsRestClassifier(LogisticRegression(max_iter=20), n_jobs=-1)
clf.fit(X_train,y_train)

del X_train
del y_train

preds=clf.predict_proba(X_test)

del clf
del X_test

num = int(np.ceil(num_users * 0.05))
ans_inds = np.argsort(preds)

last_3weeks = train.loc[mask_test].loc[train.loc[mask_test].date >= train.loc[mask_test].date.max() - 21 + 1]
y_not = last_3weeks.groupby('user_id').id3.apply(set)

del train
del mask_test
del last_3weeks

y_pred = {}
sorted_data = []
dicts = []

def get_preds(user):
    items_not = y_not.get(user, [])
    items_pred = []
    i = 1
    prob = 1
    while len(items_pred) < 5 and i < features_shape[1]:
        if not ans_inds[user, -i] in items_not:
            items_pred += [temp3[ans_inds[user, -i]]]
            prob *= 1 - preds[user, ans_inds[user, -i]]
    
        i += 1
    if len(items_pred) < 5:
        prob = 1
    return ((prob, user), {user: items_pred})

sorted_data = np.array(Parallel(n_jobs = -1, verbose=50)(delayed(get_preds)(i) for i in range(features_shape[0])))
dicts = sorted_data[:, 1]
sorted_data = list(sorted_data[:, 0])

del preds

sorted_data = sorted(sorted_data)

for pair in sorted_data[:num]:
    y_pred.update(dicts[pair[1]])

if sys.argv[1] == 'test':
    sys.path.append('../')
    from scorer import scorer
    print(scorer(y_val_dict, y_pred, num_users))
else:
    y_pred_df = pd.DataFrame.from_records(y_pred).T.reset_index()
    y_pred_df.columns = ['user_id', 'id3_1', 'id3_2', 'id3_3', 'id3_4', 'id3_5']

    y_pred_df.to_csv('submission.csv', index=False)

print(time() - t)